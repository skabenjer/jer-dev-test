<?php

use Phinx\Migration\AbstractMigration;

class TblMessages extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
     public function up()
     {
         $components_table = $this->table('tbl_messages');
         $components_table->addColumn('message', 'string')
              ->addColumn('status', 'enum',
              array(
                  'values'  => ['posted','deleted'],
                  'default' => 'posted'
              ))
              ->create();
     }

     public function down() {
         $this->dropTable('tbl_messages');
     }
}
