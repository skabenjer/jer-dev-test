<?php
//========================================
// CORS
//========================================
if (isset($_SERVER['HTTP_ORIGIN'])) {
  header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
  header('Access-Control-Allow-Credentials: true');
  header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
  if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

  if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
    header("Access-Control-Allow-Headers:{$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

  exit(0);
}

//========================================
// Routes
//========================================

//generate default data for table
$app->get('/api', function ($request, $response) {
    $this->logger->addInfo("Message List");
    $sql = "SELECT id, message
        from tbl_messages where status='posted'";
    $stmt = $this->db->query($sql);
    $results = $stmt->fetchAll();
    return json_encode($results);
});

//create new message
$app->post('/api/new', function ($request, $response) {
    $data = $request->getParsedBody();
    $message_data = [];
    $message_data['id'] = "";
    $message_data['message'] = filter_var($data['message'], FILTER_SANITIZE_STRING);

    $sql = "insert into tbl_messages
        (message) values
        (:message)";

    $stmt = $this->db->prepare($sql);
    $result = $stmt->execute([
        "message" => $message_data['message']
    ]);

    if(!$result) {
        throw new Exception("could not save record");
    }

    $response = "Message Added";
    return $response;
});

//edit existing message
$app->post('/api/edit', function ($request, $response) {
    $data = $request->getParsedBody();
    $message_data = [];
    $message_data['id'] = filter_var($data['id'], FILTER_SANITIZE_STRING);
    $message_data['message'] = filter_var($data['message'], FILTER_SANITIZE_STRING);

    $sql = "update tbl_messages set message = :message where id = :id";

    $stmt = $this->db->prepare($sql);
    $result = $stmt->execute([
        "id" => $message_data['id'],
        "message" => $message_data['message']
    ]);

    if(!$result) {
        throw new Exception("could not update record");
    }

    $response = "Edited Message";
    return $response;
});

//delete message
$app->post('/api/delete', function ($request, $response) {
    $data = $request->getParsedBody();
    $message_data = [];
    $message_data['id'] = filter_var($data['id'], FILTER_SANITIZE_STRING);
    $message_data['message'] = "";

    $sql = "update tbl_messages set status = 'deleted' where id = :id";

    $stmt = $this->db->prepare($sql);
    $result = $stmt->execute([
        "id" => $message_data['id']
    ]);

    if(!$result) {
        throw new Exception("could not delete record");
    }

    $response = "Deleted Message";
    return $response;
});

//return data based on like query
$app->post('/api/search', function ($request, $response) {
    $data = $request->getParsedBody();
    $message_data = [];
    $message_data['id'] = "";
    $message_data['message'] = filter_var($data['message'], FILTER_SANITIZE_STRING);
    $message = $message_data['message'];
    $sql = "SELECT id, message from tbl_messages where status='posted' and message LIKE :message";
    $stmt = $this->db->prepare($sql);
    $result = $stmt->execute(array(':message' => '%'.$message.'%'));
    $results = $stmt->fetchAll();
    return json_encode($results);
});
