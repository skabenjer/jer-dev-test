<!DOCTYPE html>
<html>
<head>
  <title>Message Boards</title>
  <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
  <script src="js/jquery-3.1.1.min.js"></script>
</head>
<body>
  <table id="msgboard" class="pure-table">
    <tr>
        <th>Message</th>
        <th>Actions</th>
    </tr>
    <!--Dynamically generated-->
    <!--Message Content-->
    <!--Button for edit and delete-->
  </table>
  <br/>
  <form id="searchForm" class="pure-form">
      <!--Searching-->
      <label for="search">Search</label>
      <input type="text" name="search" id="search">
      <input type="button" id="searchbutton" class="pure-button" value="Go" />
  </form>
  <br/>
  <form id="editForm" class="pure-form">
      <!--Editing-->
      <label for="edit">Edit</label>
      <input type="text" name="edit" id="edit">
      <input type="hidden" id="hiddenid" name="hiddenid" value="">
      <input type="button" id="editbutton" class="pure-button" value="Edit" />
  </form>
  <br/>
  <form id="createForm" class="pure-form">
      <!--Composing-->
      <label for="message">Message</label>
      <input type="text" name="message" id="message">
      <input type="button" id="createbutton" class="pure-button" value="Create">
  </form>
  <!--Javascript here-->
  <script>
    $(document).ready(function(){
      //========================================
      // Generate table on load
      //========================================
        generateTable();

      //========================================
      // Click Events
      //========================================

        $('#msgboard').on('click', '.editbutton', function(){
            var hiddenval = $(this).val();
            $('#hiddenid').val(hiddenval);
            $('#edit').val($(this).closest('td').prev().text());
        });

        $('#msgboard').on('click', '.deletebutton', function(){
            var hiddenval = $(this).val();
            deleteMsg(hiddenval);
            alert('Deleted Message.')
        });

        $("#createbutton").click(function(){
          if($('#message').val() == ""){
            alert('Please fill up the message box.');//if clicked create when empty textbox
            $(this).closest('form').find("input[type=text]").val("");//reset form
          }else{
            var values = {};
            $.each($('#createForm').serializeArray(), function(i, field) {
                values[field.name] = field.value;
            });
            createMsg(values['message']);
            $(this).closest('form').find("input[type=text]").val("");//reset form
          }
        });

        $("#editbutton").click(function(){
          if($('#hiddenid').val() == ""){
            alert('No message selected.');//if clicked edit when no message selected
            $(this).closest('form').find("input[type=text],input[type=hidden]").val("");//reset form
          }else{
            var values = {};
            $.each($('#editForm').serializeArray(), function(i, field) {
                values[field.name] = field.value;
            });
            editMsg(values['hiddenid'],values['edit']);
            $(this).closest('form').find("input[type=text],input[type=hidden]").val("");//reset form
          }
        });

        $("#searchbutton").click(function(){
          if($('#search').val() == ""){
            alert('Please fill up the message box.');//if clicked search when empty textbox
            $(this).closest('form').find("input[type=text]").val("");//reset form
          }else{
            var values = {};
            $.each($('#searchForm').serializeArray(), function(i, field) {
                values[field.name] = field.value;
            });
            searchMsg(values['search']);
            $(this).closest('form').find("input[type=text]").val("");//reset form
          }
        });

        //========================================
        // Ajax Functions
        //========================================

        function createMsg(message){
          //call to create new message
          $.ajax({
              type: 'POST',
              url: 'http://localhost:8080/api/new',
              data: "message="+message,
              success: function(response) {
                $("#msgboard").find("tr:gt(0)").remove();//remove table rows except first
                generateTable();//reload table
              },
          });
        }

        function editMsg(id,message){
          //call to edit message
          $.ajax({
              type: 'POST',
              url: 'http://localhost:8080/api/edit',
              data: {id: id, message: message},
              success: function(response) {
                $("#msgboard").find("tr:gt(0)").remove();//remove table rows except first
                generateTable();//reload table
              },
          });
        }

        function deleteMsg(id){
          //call to delete message
          $.ajax({
              type: 'POST',
              url: 'http://localhost:8080/api/delete',
              data: "id="+id,
              success: function(response) {
                $("#msgboard").find("tr:gt(0)").remove();//remove table rows except first
                generateTable();//reload table
              },
          });
        }

        function searchMsg(message){
          //call to generate new table based on keyword
          $.ajax({
              type: 'POST',
              url: 'http://localhost:8080/api/search',
              data: "message="+message,
              dataType: 'json',
              success: function(response) {
                $("#msgboard").find("tr:gt(0)").remove();//remove table rows except first
                //succeeding lines generate new rows
                var trHTML = '';
                $.each(response, function (i, item) {
                    trHTML += '<tr><td>' + item.message + '</td><td>' + '<button class="pure-button pure-button-primary editbutton" value=' + item.id + '>Edit</button>  <button class="pure-button pure-button-primary deletebutton" value=' + item.id + '>Delete</button>' + '</td></tr>';
                });
                $('#msgboard').append(trHTML);
              },
          });
        }

        function generateTable(){
          //call to generate new table
          $.ajax({
              url: 'http://localhost:8080/api',
              dataType: 'json',
              success: function(response) {
                var trHTML = '';
                $.each(response, function (i, item) {
                    trHTML += '<tr><td>' + item.message + '</td><td>' + '<button class="pure-button pure-button-primary editbutton" value=' + item.id + '>Edit</button>  <button class="pure-button pure-button-primary deletebutton" value=' + item.id + '>Delete</button>' + '</td></tr>';
                });
                $('#msgboard').append(trHTML);
              }
          });
        }
    });
  </script>
</body>
</html>
